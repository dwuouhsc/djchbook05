find . -print | sed -e 's;[^/]*/;|____;g;s;____|; |;g'

cd /Users/DeeWu/src/Django_Attempt01/my_blog_env/
bin/django-admin.py startproject djbookch01
../bin/python manage.py runserver


**Chapter 5:**
1) mkdir /Users/DeeWu/src/Django_Attempt01/db  (this is location for db)
cd /Users/DeeWu/src/Django_Attempt01/my_blog_env/
bin/django-admin.py startproject djbookch05
vi djbookch05/settings.py

**(in file)**
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': '/Users/DeeWu/src/Django_Attempt01/db/dbbookch05.db',                      # Or path to database file if using sqlite3.
        'USER': '',                      # Not used with sqlite3.
        'PASSWORD': '',                  # Not used with sqlite3.
        'HOST': '',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
    }
}


(start virtualenv python)
../bin/python  (DOESN"T WORK) run
../bin/python manage.py shell


#
# Code snippet 5 #########################################################
# NOW RUNS

>>> from django.db import connection
>>> cursor = connection.cursor()



NOW let's get the app working within
../bin/python manage.py startapp books

**remember to edit **


INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # Uncomment the next line to enable the admin:
    # 'django.contrib.admin',
    # Uncomment the next line to enable admin documentation:
    # 'django.contrib.admindocs',
   'books',
)

because I put books in djbookch05 the above works,
but if I put app in djbookch05/djbookch05 I would do below:
   'djbookch05.books',
)

find . -print | sed -e 's;[^/]*/;|____;g;s;____|; |;g'
.
|____books
| |______init__.py
| |____models.py
| |____tests.py
| |____views.py
|____djbookch05
| |______init__.py
| |______init__.pyc
| |____settings.py
| |____settings.pyc
| |____urls.py
| |____wsgi.py
|____manage.py


(now edit models.py)

**change all maxlength to max_length in model**


http://stackoverflow.com/questions/4382032/defining-a-model-class-in-django-shell-fails


../bin/python manage.py validate
Error: One or more models did not validate:
books.author: "headshot": To use ImageFields, you need to install the Python Imaging Library. Get it at http://www.pythonware.com/products/pil/ .

i
downloaded 
/Users/DeeWu/src/Django_Attempt01/

i618  cp -r /Users/DeeWu/Downloads/Imaging-1.1.7  .
  tar xvfz Imaging-1.1.7.tar.gz
  cd Imaging-1.1.7
  python setup.py install
  ../my_blog_env/bin/python setup.py install`


**this is why its different to set up models in interactive terminal**
http://stackoverflow.com/questions/4382032/defining-a-model-class-in-django-shell-fails

OK I got to this point:
../bin/python manage.py validate0 errors found


** NOW create tables with**
../bin/python manage.py sqlall books

which produces the output:
BEGIN;
CREATE TABLE "books_publisher" (
    "id" integer NOT NULL PRIMARY KEY,
    "name" varchar(30) NOT NULL,
    "address" varchar(50) NOT NULL,
    "city" varchar(60) NOT NULL,
    "state_province" varchar(30) NOT NULL,
    "country" varchar(50) NOT NULL,
    "website" varchar(200) NOT NULL
)
;
CREATE TABLE "books_author" (
    "id" integer NOT NULL PRIMARY KEY,
    "salutation" varchar(10) NOT NULL,
    "first_name" varchar(30) NOT NULL,
    "last_name" varchar(40) NOT NULL,
    "email" varchar(75) NOT NULL,
    "headshot" varchar(100) NOT NULL
)
;
CREATE TABLE "books_book_authors" (
    "id" integer NOT NULL PRIMARY KEY,
    "book_id" integer NOT NULL,
    "author_id" integer NOT NULL REFERENCES "books_author" ("id"),
    UNIQUE ("book_id", "author_id")
)
;
CREATE TABLE "books_book" (
    "id" integer NOT NULL PRIMARY KEY,
    "title" varchar(100) NOT NULL,
    "publisher_id" integer NOT NULL REFERENCES "books_publisher" ("id"),
    "publication_date" date NOT NULL
)
;
CREATE INDEX "books_book_22dd9c39" ON "books_book" ("publisher_id");
COMMIT;

../bin/python manage.py syncdb
Creating tables ...
Creating table auth_permission
Creating table auth_group_permissions
Creating table auth_group
Creating table auth_user_user_permissions
Creating table auth_user_groups
Creating table auth_user
Creating table django_content_type
Creating table django_session
Creating table django_site
Creating table books_publisher
Creating table books_author
Creating table books_book_authors
Creating table books_book

You just installed Django's auth system, which means you don't have any superusers defined.
Would you like to create one now? (yes/no): yes
Username (leave blank to use 'deewu'): deewu
E-mail address: dee-wu@ouhsc.edu
Password:  (common name in house)
Password (again): 
Superuser created successfully.
Installing custom SQL ...
Installing indexes ...
Installed 0 object(s) from 0 fixture(s)


